package com.digisafari.sapl.user.service;

import java.util.List;

import com.digisafari.sapl.user.exception.UserAlreadyExistsException;
import com.digisafari.sapl.user.exception.UserNotFoundException;
import com.digisafari.sapl.user.model.User;

public interface IUserService {
	
	public boolean validateUser(String username, String password) throws UserNotFoundException;
	public User getUserByName(String name) throws UserNotFoundException;
	public User getUserByUsername(String username) throws UserNotFoundException;
	public List<User> getAllUsers();
	public User getUserById(String id) throws UserNotFoundException;
	public User saveUser(User user) throws UserAlreadyExistsException;
	public boolean deleteUser(String userId) throws UserNotFoundException;
	public User updateUser(User user) throws UserNotFoundException;

}
