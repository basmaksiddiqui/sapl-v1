package com.digisafari.sapl.user.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.digisafari.sapl.user.model.User;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends MongoRepository<User, String> {

	/*
	 * CRUD methods based on id property findById(String id);
	 */

	// Query Methods

	// SQL
	// "select username, password from user_details where username ='abcd' and
	// password = 'abcd'"
	@Query("{'username': {$in : [?0]},'password': {$in : [?1]}}")
	public User validateUser(String username, String password);
	// public List<Course> userCourses(String id);

	@Query("{'username': {$in : [?0]}}")
	public User findByusername(String username);

	@Query("{'name': {$in : [?0]}}")
	public User findByname(String name);

}
