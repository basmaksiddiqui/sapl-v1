package repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import model.Quiz;

@Repository
public interface QuizRepository extends MongoRepository<Quiz,String> {
	//Query Methods
}
